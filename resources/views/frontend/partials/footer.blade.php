<div class="footer">
  <div class="container">
    <br>
    <div class="row">
      <div class="col-md-4 col-xs-12 col-footer">
        <h4>
        Sekretariat
        </h4>
        <p>
          Ruang Kemahasiswaan Gedung D3 Lantai 1 
          Politeknik Elektronika Negeri Surabaya 
          Jalan Raya ITS Sukolilo, Surabaya 60111 
          <br>
          Tlp : +62-31-5947280 (hunting) <br>
          Fax : +62-31-5946114 <br>
          e-mail : <a href="mailto:kmipn@pens.ac.id" style="color:#fff">kmipn@pens.ac.id</a> <br>
          URL : <a href="http://www.kmipn.pens.ac.id" style="color:#fff">http://www.kmipn.pens.ac.id</a> 
        </p>
      </div>
      <div class="col-md-4 col-xs-12 col-footer">
        <h4>  
          Sponsorship
        </h4>
      </div>
      <div class="col-md-4 col-xs-12 col-footer">
        <h4>
          Supported by
        </h4>
      </div>
    </div>
    <br>
    <br>
    <center>
        <span class="s18">KMIPN | Kompetisi Mahasiswa Bidang Informatika Politeknik Nasional 2018</span>
        <br>
        <span class="s16">
        Politeknik Elektronika Negeri Surabaya
        </span>
    </center>
  </div>
</div>
