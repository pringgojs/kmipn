<?php
namespace App\Http\Controllers\Auth;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AdminLoginController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '/ecodeeepis';

    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function logout()
    {
        Auth::logout();
        \Session::flush();

        return redirect('/')->with('message', array(
          'title' => 'Yay!',
          'type' => 'success',
          'msg' => 'You\'re log out.',
        ));
    }
}